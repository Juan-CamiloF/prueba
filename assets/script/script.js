const seccion = document.querySelectorAll(".seccion");
function secciones(e) {
  for (let i = 0; i < seccion.length; i++) {
    if (e == seccion[i].id) {
      seccion[i].className = `${e}`;
    } else {
      seccion[i].className = `invisible`;
    }
  }
}
//Barra
const btnIcon = document.getElementById("icono");
const barra = document.getElementById("barra");
let i = 0;
//Evento de menú
btnIcon.addEventListener("click", () => {
  if (i == 0) {
    barra.className = "barra dos";
    btnIcon.innerHTML = `&#10006`;
    i = 1;
  } else {
    barra.classList.remove("dos");
    barra.className = "barra uno";
    btnIcon.innerHTML = `&#9776`;
    i = 0;
  }
  cerrarBarra(i);
});
//Cerrar el menú cuando se de click a un btn
const btn = document.querySelectorAll('.btn');
function cerrarBarra(estado) {
  if (estado == 1) {
    for (let i = 0; i < btn.length; i++) {
      btn[i].addEventListener('click',barraCerrada)
    }
  }
}
//Funcion para quitar el menu
function barraCerrada() {
  barra.classList.remove("dos");
  barra.className = "barra uno";
  btnIcon.innerHTML = `&#9776`;
  i = 0;
}
